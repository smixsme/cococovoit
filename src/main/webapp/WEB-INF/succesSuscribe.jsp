<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>


<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">
<meta charset="UTF-8">
<title>Inscription</title>
<link rel="stylesheet" href="css/style.css">
<link rel="shortcut icon"
	href="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png"
	type="image/png">
</head>
<body>
	<header>
		<a href="acceuil.jsp"><img id="logo" alt="Logo" src="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png"></a>

		<div id="debanniere">
			<div class="ban"><a href="acceuil.jsp">Deconnexion</a></div>

		</div>
		<img id="user" src="http://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png">
	</header>
	<div id="slogan">
		<h1>Roulez Radin !!!</h1>
		<h2>Bienvenue  <c:out value="${sessionScope.pseudo}" /></h2>
		<p>Vous êtes inscrit  <c:out value="${sessionScope.pseudo}" /></p>
	</div>
	<footer id="bis">
	<div id="gif">
	<img id="poisson" src="images/illustration_fishbowl-3f0df5c01684b92edb912658423a07d1760b3acaf80860d6ded272aa232b8516.gif"></div></footer>
	
	
	

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>