<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>


<!DOCTYPE html>
<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<meta charset="UTF-8">
	<title>Roulez Radin</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="shortcut icon" href="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png" type="image/png">
</head>
<body>
	<header>
		<a href="acceuil.jsp"><img id="logo" alt="Logo" src="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png"></a>

		<div id="banniere">
			<div class="ban"><a href="pageInscription.jsp">Inscription</a></div>
			<div class="ban"><a href="connection.jsp">Connexion</a></div>

		</div>
		<img id="user" src="http://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png">
	</header>
	<div id="slogan">
		<h1>Roulez Radin !!!</h1>
		<h2>parce que l'écolo c'est bien surtout quand ça coûte rien !</h2>
	</div>
	<form method="get" action="Chercher">
		<div>
			<label id="depart">Ville de départ :</label>
			<p>
				<input type="text" name="x" />
			</p>
		</div>
		<div>
			<label id="arrivee">Ville d'Arrivée :</label>
			<p>
				<input type="text" name="y" />
			</p>
		</div>

		<input type="submit" value="Recherche" />
	</form>
	<footer>
		<article id="a1">
			<div class="boiteaimage"><img class="photo" id="b1" alt="Logo" src="https://media.firerank.com/upload/dli/2048/5540fc6d5cb7b.jpg"></img></div>
			<div>
				<h3>Richard Lugner (62ans) et Cathy Schmitz (27ans)</h3>
				<h5>Depuis qu'on partage les frais de trajet avec notre chauffeur, on a fait des économie significatives.
				"Roulez Radin" c'est vraiment trop bien !</h5>
			</div>
		</article>
		<article id="a2">
			<div class="boiteaimage"><img class="photo" id="b2" alt="Logo" src="https://previews.123rf.com/images/william87/william871210/william87121000200/16275567-hippie-gruppe-trampen-auf-einer-landstra%C3%9Fe.jpg"></img></div>
			<div>
				<h3>Des Chercheurs Américains ont fait une Découverte Révolutionnaire</h3>
				<h5>Plus vous entassez de hippies dans votre voiture, moins votre trajet vous coûte cher. Et mieu que ça, au delà de 3 hippies votre trajet devient rentable !</h5>
			</div>
		</article>
	</footer>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>