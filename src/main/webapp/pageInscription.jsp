<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>


<!DOCTYPE html>
<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat"
	rel="stylesheet">
<meta charset="UTF-8">
<title>Inscription</title>
<link rel="stylesheet" href="css/style.css">
<link rel="shortcut icon"
	href="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png"
	type="image/png">
</head>
<body>
	<header>
		<a href="acceuil.jsp"><img id="logo" alt="Logo"
			src="http://blog.dekra-norisko.fr/wp-content/uploads/2016/02/Dekra-GagnerArgent.png"></a>

		<div id="banniere">
			<div class="ban">
				<a href="pageInscription.jsp">Inscription</a>
			</div>
			<div class="ban">
				<a href="connection.jsp">Connexion</a>
			</div>

		</div>
		<img id="user"
			src="http://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png">
	</header>
	<div id="slogan">
		<h1>Roulez Radin !!!</h1>
		<h2>Changez de vie dès maintenant en vous faisant du blé sur le
			dos de jeunes ne pouvant pas se payer la SNCF !</h2>
	</div>

	<div id="bleu">
		<div>
			<img id="piece"
				src="http://highviewgarage.co.uk/wp-content/uploads/2017/10/Car-Service-Money-Saving.png">
		</div>
		<div id="inscrip">
			<div>
				<p>Bonjour, veillez renseigner ce formulaire pour vous inscrire</p>
			</div>
			<form method="get" action="Inscription">
				<div>
					<fieldset>
						<p>
							<label for="pseudo">Pseudo :</label> <input type="text"
								id="pseudo" name="pseudo" placeholder="Pseudo"
								required="required" />
							<c:out value="${pseudoExiste}" />
						</p>

						<p>
							<label for="Mot de Passe">Mot de Passe :</label> <input
								type="text" id="mdp" name="Mot de Passe"
								placeholder="Mot de Passe" required="required" />
							<c:out value="${mdpNonCorrecte}" />
						</p>

						<p>
							<label for="nom">Nom :</label> <input type="text" id="nom"
								name="nom" placeholder="Nom" required="required" />
						</p>

						<p>
							<label for="prenom">Prenom :</label> <input type="text"
								id="prenom" name="prenom" placeholder="Prenom"
								required="required" />
						</p>


						<p>
							<label for="email">E-mail :</label> <input type="email"
								id="email" name="email" placeholder="E-mail" required="required" />
						</p>
						<p>
							<label for="voiture">Type de voiture :</label> <input type="text"
								id="voiture" name="voiture" placeholder="Type de voiture" />
						</p>
					</fieldset>
					<input type="submit" value="S'enregistrer" />
				</div>
			</form>
		</div>
	</div>


	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>