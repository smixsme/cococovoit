package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the voyage database table.
 * 
 */
@Entity
@NamedQuery(name="Voyage.findAll", query="SELECT v FROM Voyage v")
public class Voyage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String arrivedTown;

	@Temporal(TemporalType.DATE)
	private Date schedule;

	private String startTown;

	//bi-directional many-to-many association to User
	@ManyToMany
	@JoinTable(
		name="totravel"
		, joinColumns={
			@JoinColumn(name="voyage")
			}
		, inverseJoinColumns={
			@JoinColumn(name="user")
			}
		)
	private List<User> users;

	public Voyage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArrivedTown() {
		return this.arrivedTown;
	}

	public void setArrivedTown(String arrivedTown) {
		this.arrivedTown = arrivedTown;
	}

	public Date getSchedule() {
		return this.schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}

	public String getStartTown() {
		return this.startTown;
	}

	public void setStartTown(String startTown) {
		this.startTown = startTown;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}