package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String car;

	private String emailAdresse;

	private String firstName;

	private String lastName;

	private String mDP;

	private String pseudo;

	//bi-directional many-to-many association to Voyage
	@ManyToMany(mappedBy="users")
	private List<Voyage> voyages;

	public User() {
	}
	
	
	
	
	// Constructeur d'un utilisateur AVEC voiture
	public User(String car, String emailAdresse, String firstName, String lastName, String mDP, String pseudo) {
		super();
		this.car = car;
		this.emailAdresse = emailAdresse;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mDP = mDP;
		this.pseudo = pseudo;
	}





	// Constructeur d'un utilisateur SANS voiture
	public User(String emailAdresse, String firstName, String lastName, String mDP, String pseudo) {
		this.emailAdresse = emailAdresse;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mDP = mDP;
		this.pseudo = pseudo;
	}






	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCar() {
		return this.car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public String getEmailAdresse() {
		return this.emailAdresse;
	}

	public void setEmailAdresse(String emailAdresse) {
		this.emailAdresse = emailAdresse;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMDP() {
		return this.mDP;
	}

	public void setMDP(String mDP) {
		this.mDP = mDP;
	}

	public String getPseudo() {
		return this.pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public List<Voyage> getVoyages() {
		return this.voyages;
	}

	public void setVoyages(List<Voyage> voyages) {
		this.voyages = voyages;
	}




	@Override
	public String toString() {
		return "User [id=" + id + ", car=" + car + ", emailAdresse=" + emailAdresse + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", mDP=" + mDP + ", pseudo=" + pseudo + ", voyages=" + voyages + "]";
	}

	
	
	
}