package servlets;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.x.protobuf.MysqlxConnection.Close;

import model.User;






/**
 * Servlet implementation class Inscription
 */
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Inscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		

		// Création d'un manager pour gérer les transactions avec la BDD
		EntityManager manager = Persistence.createEntityManagerFactory("Cocovoit").createEntityManager();
		
		String adresse = "/WEB-INF/failSuscribe.jsp";
		boolean pseudoLibre = true;
		
		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adresseMail = request.getParameter("email");
		String voiture = request.getParameter("voiture");
		String mDP = request.getParameter("mdp");
	
		//Crée une instance d'User. Si il a une voiture, voire prochain IF
		User newUser = new User(adresseMail, prenom, nom, mDP, pseudo);
		
		//Si l'utilisateur à une voiture, on lui rajoute.	
		if (voiture != null) {
			newUser.setCar(voiture);
		}
	
		
		
//		Requête pour compter le nombre d'utilisateurs en BDD
//		SELECT COUNT(username) FROM users WHERE username = "$username"
		Query demande = manager.createQuery("SELECT COUNT(u) FROM User u WHERE u.pseudo = :username");
		demande.setParameter("username", newUser.getPseudo());
		
		
		
		
		// On vérifie que le pseudo n'existe pas dans la base.
		if ( Long.parseLong(demande.getSingleResult().toString()) != 0) {
			pseudoLibre = false;
			request.setAttribute("pseudoDoNotExist", "Veuillez choisir un autre pseudo");
			
		}else {
			pseudoLibre = true;
			adresse ="/WEB-INF/succesSuscribe.jsp";
		
			manager.getTransaction().begin();
			manager.persist(newUser);
			manager.getTransaction().commit();
			
			
		}
		
//		for (User i : BaseDeDonnee.getUsers()) {
//
//			if (pseudo.equalsIgnoreCase(i.getPseudo())) {
//				
//				// Si l'utilisateur existe, onl'envoie se faire foutre
//				
//				//TODO a codé
//				pseudoLibre = false;
//			}
//		}

//		if (pseudoLibre && voiture == null) {
//			User nouvelUtilisateur = new User(pseudo, nom, prenom, adresseMail, mDP);
//			BaseDeDonnee.getUsers().add(nouvelUtilisateur);
//		}else {
//			User nouvelUser = new User(pseudo, nom, prenom, adresseMail, mDP, voiture);
//			BaseDeDonnee.getUsers().add(nouvelUser);
//		}
		
		
		
		
		

		this.getServletContext().getRequestDispatcher(adresse).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
