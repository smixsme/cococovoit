package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BaseDeDonnee;
import beans.User;

/**
 * Servlet implementation class Connection
 */
public class Connection extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Connection() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String pseudo = request.getParameter("pseudo");
		String mDP = request.getParameter("pass");
		String adresse = "/WEB-INF/failConnect.jsp";

		// Verifie que l'utilisateur existe et que le pseudo est correcte
		// Si oui, on va vers la page connect.jsp
		// Si non, on garde l'adresse qui envoie l'utilisateur se faire mettre
		for (User i : BaseDeDonnee.getUsers()) {

			if (pseudo.equalsIgnoreCase(i.getPseudo()) && (mDP.equalsIgnoreCase(i.getmDP()))) {
				adresse = "/WEB-INF/connect.jsp";
				// Si l'utilisateur existe, on crée une session et on attribue le pseudo de
				// l'utilisateur
				HttpSession session = request.getSession();
				session.setAttribute("pseudo", pseudo);
			}
		}

		this.getServletContext().getRequestDispatcher(adresse).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
